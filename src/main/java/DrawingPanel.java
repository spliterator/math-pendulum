import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import java.awt.*;

class DrawingPanel extends JPanel {

    DrawingPanel() {
        this.setBorder(new EtchedBorder());
    }

    void drawPendulum() {
        synchronized (object) {
            Graphics graphics = getGraphics();
            double max = 0;
            int x = (int) (Math.sin(alpha) * lenght);
            int y = (int) ((lenght - Math.cos(alpha) * lenght));
            graphics.setColor(Color.black);
            graphics.drawLine(startPoint.x, startPoint.y, x + startPoint.x, -y +lenght+ startPoint.y);
            graphics.drawOval( x+startPoint.x,  -y+lenght+startPoint.x, 3, 3);
            try {
                Thread.sleep(1);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }

    void clear()
    {
        Graphics graphics = getGraphics();
        graphics.clearRect(0,0,getWidth(),getHeight());
    }

    void setLenght(int lenght) {
        this.lenght = lenght;
    }

    void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    private final Point startPoint = new Point(30,30);
    private int lenght = 0;
    private double alpha = 0;
    private final Object object = new Object();

}
