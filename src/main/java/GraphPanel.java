import javax.swing.*;
import java.awt.*;

class GraphPanel extends JPanel {

    GraphPanel()
    {
        setPreferredSize(new Dimension(700,200));
    }

    void paintGrid()
    {
        Graphics g = getGraphics();
        g.setColor(Color.PINK);

        for (int i = 0; i< getWidth(); i++)
        {
            int j = i*5;
            g.drawLine(j,0,j,getHeight());
        }
        for (int i = 0; i< getHeight(); i++)
        {
            int j = i*5;
            g.drawLine(0,j,getWidth(),j);
        }
    }
    void paintPoint(Point aPoint)
    {
        Graphics g = getGraphics();
        g.setColor(Color.BLUE);
        g.drawOval(aPoint.x,aPoint.y,3,3);
    }
}
