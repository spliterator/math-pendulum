import com.borland.jbcl.layout.VerticalFlowLayout;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;

class MainFrame extends JFrame {

    MainFrame() {
        init();
    }

    private void init() {
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.setPreferredSize(new Dimension(600, 400));
        this.add(mainPanel);

        JPanel centerPanel = new JPanel(new VerticalFlowLayout());
//        centerPanel.setBackground(Color.red);

        JPanel settings = new JPanel(new VerticalFlowLayout());
//        settings.setBackground(Color.blue);
        settings.setPreferredSize(new Dimension(150, 100));

        JPanel lengthPanel = new JPanel(new BorderLayout(5,5));
        lengthPanel.add(new JLabel("length"), BorderLayout.WEST);
        lengthPanel.add(length, BorderLayout.CENTER);
        settings.add(lengthPanel);

        JPanel stepPanel = new JPanel(new BorderLayout(5,5));
        stepPanel.add(new JLabel("steps"), BorderLayout.WEST);
        stepPanel.add(steps, BorderLayout.CENTER);
        steps.setValue(2500);  // 10 -4 80
        settings.add(stepPanel);

        JPanel startPanel = new JPanel(new BorderLayout(5,5));
        startPanel.add(new JLabel("start"), BorderLayout.WEST);
        startPanel.add(start, BorderLayout.CENTER);
        settings.add(startPanel);

        usePID.setSelected(true);
        usePID.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                kP.setEnabled(usePID.isSelected());
                kI.setEnabled(usePID.isSelected());
                kD.setEnabled(usePID.isSelected());
            }
        });
        settings.add(usePID);

        JPanel PIDPanel = new JPanel(new VerticalFlowLayout());
        PIDPanel.setBorder(new TitledBorder("PID"));

        JPanel kpPanel = new JPanel(new BorderLayout(5,5));
        kpPanel.add(new JLabel("KP"),BorderLayout.WEST);
        kpPanel.add(kP,BorderLayout.CENTER);
        PIDPanel.add(kpPanel);
        JPanel kiPanel = new JPanel(new BorderLayout(5,5));
        kiPanel.add(new JLabel("KI"),BorderLayout.WEST);
        kiPanel.add(kI,BorderLayout.CENTER);
        PIDPanel.add(kiPanel);
        JPanel kdPanel = new JPanel(new BorderLayout(5,5));
        kdPanel.add(new JLabel("KD"),BorderLayout.WEST);
        kdPanel.add(kD,BorderLayout.CENTER);
        PIDPanel.add(kdPanel);
        settings.add(PIDPanel);


        mainPanel.add(centerPanel, BorderLayout.CENTER);
        mainPanel.add(settings, BorderLayout.EAST);

        drawingPanel.setPreferredSize(new Dimension(200,200));
//        drawingPanel.setMinimumSize(new Dimension(200,200));
//        drawingPanel.setMaximumSize(new Dimension(200,200));
//        drawingPanel.setSize(new Dimension(200,200));

        drawingPanel.setLenght(100);
        centerPanel.add(drawingPanel,BorderLayout.NORTH);

        JButton run = new JButton("run");
        run.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {

                graphPanel.paintGrid();
                if (usePID.isSelected()) {
                    mathPendulum.usePID((Integer) steps.getValue());
                }
                for (int i = 0; i<(int) steps.getValue(); i++)
                {
                    double v;
                    if (usePID.isSelected()) {
                        v = mathPendulum.pidfunk[i];
                    } else {
                        v = mathPendulum.doStep();
                    }
                    drawingPanel.setAlpha(v);
                    drawingPanel.drawPendulum();
                    System.out.println(v);
                    graphPanel.paintPoint(new Point(i/2,(int)( v*30)+graphPanel.getHeight()/2));
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    drawingPanel.clear();
                }


            }
        });
        settings.add(run);

        length.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                mathPendulum.setLenght((Integer) length.getValue());
                drawingPanel.setLenght((Integer) length.getValue());
            }
        });

        start.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                mathPendulum.setStart(Math.PI/(int)(start.getValue()));
            }
        });
        JScrollPane pane = new JScrollPane(graphPanel);
        centerPanel.add(pane,BorderLayout.SOUTH);

        steps.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                graphPanel.setSize(new Dimension((Integer) steps.getValue(),100));
                graphPanel.setPreferredSize(new Dimension((Integer) steps.getValue(),100));
                graphPanel.setMinimumSize(new Dimension((Integer) steps.getValue(),100));
//                graphPanel.setBackground(Color.green);
//                System.out.println("lalalallala stepchanged");
            }
        });

        kP.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                mathPendulum.kp = Double.parseDouble(String.valueOf(kP.getValue()))/1000.0;
            }
        });
        kD.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                mathPendulum.kd = Double.parseDouble(String.valueOf(kD.getValue()))/1000.0;
            }
        });
        kI.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                mathPendulum.ki = Double.parseDouble(String.valueOf(kI.getValue()))/1000.0;
            }
        });
        this.pack();
    }

    private JSpinner length = new JSpinner();
    private JSpinner start = new JSpinner();
    private JSpinner steps = new JSpinner();
    private JRadioButton usePID = new JRadioButton("use PID");
    private JSpinner kP = new JSpinner();
    private JSpinner kI = new JSpinner();
    private JSpinner kD = new JSpinner();
    private DrawingPanel drawingPanel = new DrawingPanel();
    private GraphPanel graphPanel = new GraphPanel();
    private MathPendulum mathPendulum = new MathPendulum(100,Math.PI/2, 0.1);
}
